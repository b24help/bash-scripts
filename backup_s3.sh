#!/usr/bin/env bash

#
# Backup site to S3 cloud (c) info@b24.help, 2018
#
# Install AWS CLI:
# https://docs.aws.amazon.com/cli/latest/userguide/installing.html
# 1. sudo yum -y install python-pip
# 2. pip install awscli --upgrade --user
#

# Common params

DEBUG=false
ABSOLUTE_FILENAME=`readlink -e "$0"`
DIRECTORY=`dirname "$ABSOLUTE_FILENAME"`
DISKFREE=`df ${DIRECTORY}|grep dev|awk '{print $4}'`
CONFIG="${DIRECTORY}/backup.cfg"
FOLDER_DATE="$(date +'%Y-%m-%d')"
BACKUP_TARGET="${PWD}"
MAIL_ACCOUNT="default"
RETENTION_DAYS=0

# Set backup options

OPTdb=No
OPTwww=No
OPTupload=No
OPTsync=No
OPTsample=No

MAKE_DIR=No

for arg in "$@"
do
	case "$arg" in
		--db)
			OPTdb=Yes
			MAKE_DIR=Yes
		;;
		--www)
			OPTwww=Yes
			MAKE_DIR=Yes
		;;
		--upload)
			OPTupload=Yes
		;;
		--sync)
			OPTsync=Yes
		;;
		--all)
			OPTdb=Yes
			OPTwww=Yes
			OPTsync=Yes
			OPTupload=Yes
			MAKE_DIR=Yes
		;;
		--sample)
			OPTsample=Yes
		;;
		*) CONFIG="$arg"
	esac
done

if [ $# -eq 0 ]
then
	echo "Backup site to S3 cloud (c) info@b24.help, 2018"
	echo "usage: backup_s3.sh ([--db|--www|--sync|--upload] | --all | --sample) [config_file]"
	echo "default config file - backup.cfg in same folder"
	exit
fi

if [[ "$OPTsample" = "Yes" ]]
then
cat << EOF
# Sample backup config

# Notify
MAIL_TO="your@email"
MAIL_ACCOUNT="default"

# Backup source
BACKUP_WWW="/home/bitrix/www"
BACKUP_TARGET="/home/bitrix/backup"
BACKUP_DB_ignore_table=("b_search_content" "b_search_content_freq" "b_search_content_param" "b_search_content_right" "b_search_content_site" "b_search_content_stem" "b_search_content_text" "b_search_content_title")
BACKUP_DB_Name="***"
BACKUP_DB_Login="***"
BACKUP_DB_Password="***"
TAR_exclude=("./upload/*" "./bitrix/cache/*" "./bitrix/managed_cache/*" "./bitrix/stack_cache/*" "./bitrix/backup/*")

# Backup target (AWS_S3_END_POINT required only for *.hotbox.ru)
AWS_S3_BUCKET="***"
AWS_S3_END_POINT="" 
AWS_ACCESS_KEY_ID=***
AWS_SECRET_ACCESS_KEY=***

# Delete older than N days (0 - disable)
RETENTION_DAYS=5
EOF
exit
fi

if [ ! -f $CONFIG ]
then
	echo "Config file ${CONFIG} not found"
	exit
fi

echo "Backup site to S3 cloud with config file ${CONFIG}"

. $CONFIG

function throwError()
{
	echo "Error!"
	if [[ ${DEBUG} != true && -n ${MAIL_TO} ]]; then
		MAIL_MESSAGE_HEAD="Subject: Backup [${MAIL_ACCOUNT}] error\nTo: ${MAIL_TO}\nContent-Type: text/plain; charset=UTF-8\n\n"
		FILE=$BACKUP_TARGET/backup_last.log
		while read line; do
		MAIL_MESSAGE="$MAIL_MESSAGE$line\n"
		done < $FILE
		echo -e ${MAIL_MESSAGE_HEAD}${MAIL_MESSAGE} | /usr/bin/msmtp -t -i ${MAIL_TO} -a ${MAIL_ACCOUNT}
	fi
	exit 1
}

function checkExitCode()
{
	EXITCODE=$?
	if [ "$EXITCODE" -ne "0" ]; then
		echo "ExitCode: $EXITCODE"
		throwError
	fi
}

# Prepare command arguments

PWD_BACKUP="${BACKUP_TARGET}/${FOLDER_DATE}"

if ! [ -d ${BACKUP_TARGET} ]; then
	mkdir ${BACKUP_TARGET}
fi

if [ -f ${BACKUP_TARGET}/backup_last.size ]; then
	BACKUP_SIZE=`cat ${BACKUP_TARGET}/backup_last.size`
	let "BACKUP_SIZE *= 2"
	if [ "$BACKUP_SIZE" -gt "$DISKFREE" ]; then
		echo "Is not enough space on the disk. Free ${DISKFREE}, last backup ${BACKUP_SIZE}"
		df -H
		throwError
	fi
fi

if [ -f $BACKUP_TARGET/backup_last.log ]; then
	rm -f $BACKUP_TARGET/backup_last.log
fi

if [ $DEBUG != true ]; then
  exec >$BACKUP_TARGET/backup_last.log
fi

ignore_cmd=""
for i in "${BACKUP_DB_ignore_table[@]}"
do
	ignore_cmd="$ignore_cmd --ignore-table=$BACKUP_DB_Name.${i}"
done

exclude_cmd=""
for i in "${TAR_exclude[@]}"
do
	exclude_cmd="$exclude_cmd --exclude=${i}"
done

if [[ "$MAKE_DIR" = "Yes" ]]
then
	if ! [ -d ${PWD_BACKUP} ]; then
    mkdir ${PWD_BACKUP}
	fi
fi

# delete older than N days

if ! [ ${RETENTION_DAYS} -eq 0 ]; then
	ls -t -d ${BACKUP_TARGET}/*/ | tail -n+${RETENTION_DAYS} | xargs -I{} rm -rf {}
fi

# database dump

if [[ "$OPTdb" = "Yes" ]]
then
	echo "Database structure mysqldump"
	mysqldump -u${BACKUP_DB_Login} -p${BACKUP_DB_Password} --no-data ${BACKUP_DB_Name} > ${PWD_BACKUP}/dump_struct.sql
	checkExitCode
	gzip ${PWD_BACKUP}/dump_struct.sql
	checkExitCode
	echo "Database data mysqldump"
	mysqldump -u${BACKUP_DB_Login} -p${BACKUP_DB_Password} ${ignore_cmd} ${BACKUP_DB_Name} > ${PWD_BACKUP}/dump.sql
	checkExitCode
	gzip ${PWD_BACKUP}/dump.sql
	checkExitCode
	echo "Done: database"
fi

# tar www

if [[ "$OPTwww" = "Yes" ]]
then
	echo "Tar www"
	cd ${BACKUP_WWW}
	tar ${exclude_cmd} --exclude-vcs -czf ${PWD_BACKUP}/www.tar.gz .
	checkExitCode
	cd ${PWD_BACKUP}
	cd ..
	echo "Done: tar"
fi

# s3 sync prepare

if [ -d ${PWD_BACKUP} ]; then
	du -s ${PWD_BACKUP}|awk '{print $1}' > ${BACKUP_TARGET}/backup_last.size
fi

if [[ -n "${AWS_S3_END_POINT}" ]]
then
	AWS_S3_END_POINT="--endpoint-url ${AWS_S3_END_POINT}"
	echo "with ${AWS_S3_END_POINT}"
fi

export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY

# s3 backup sync

if [[ "$OPTsync" = "Yes" ]]
then
	echo "Backup S3 sync"
	~/.local/bin/aws s3 sync ${PWD_BACKUP} s3://${AWS_S3_BUCKET}/${FOLDER_DATE} ${AWS_S3_END_POINT} --no-progress
	checkExitCode
	echo "Done: backup sync"
fi

# s3 upload sync

if [[ "$OPTupload" = "Yes" ]]
then
	echo "Upload S3 sync"
	~/.local/bin/aws s3 sync ${BACKUP_WWW}/upload/ s3://${AWS_S3_BUCKET}/upload ${AWS_S3_END_POINT} --no-progress
	checkExitCode
	echo "Done: upload sync"
fi

echo "Done"
