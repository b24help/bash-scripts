#!/usr/bin/env bash

# Set clone options

OPTdb=No
OPTwww=No
OPTupload=No
OPTfix=No
CONFIG="bitrix_clone.cfg"
OPTsample=No

for arg in "$@"
do
    case "$arg" in
        --db) OPTdb=Yes
        ;;
        --www) OPTwww=Yes
        ;;
        --upload) OPTupload=Yes
        ;;
        --fix)
         OPTfix=Yes
        ;;
        --all)
         OPTdb=Yes
         OPTwww=Yes
         OPTupload=Yes
         OPTfix=Yes
        ;;
        --sample)
         OPTsample=Yes
        ;;
        *) CONFIG="$arg"
    esac
done

if [ $# -eq 0 ]
then
    echo "Clone remote site to local (c) b24@tyrtov.ru, 2018 - 2019"
    echo "usage: clone.sh ([--db|--www|--upload|--fix] | --all | --sample) path/to/config_file"
fi

if [[ "$OPTsample" = "Yes" ]]
then
cat << EOF
# Sample backup config

# Remote host
REMOTE_HOST="bitrix@some_host"
REMOTE_WWW="/home/bitrix/www"
REMOTE_PORT="22"
REMOTE_DB_ignore_table=("b_search_content" "b_search_content_freq" "b_search_content_param" "b_search_content_right" "b_search_content_site" "b_search_content_stem" "b_search_content_text" "b_search_content_title")
REMOTE_DB_sync_only="No" # if set "Yes", then skip a database structure clone 
REMOTE_DB_Name="***"
REMOTE_DB_Login="***"
REMOTE_DB_Password="***"
TAR_exclude=("./upload/*" "./bitrix/cache/*" "./bitrix/managed_cache/*" "./bitrix/stack_cache/*" "./bitrix/backup/*")
UPLOAD_exclude=("mail" "uf" "resize_cache" "tmp" "disk")

# Local host
DBLogin="****"
DBPassword="****"
DBName="****"
WWW="/home/bitrix/ext_www/some_host"
KEEP_LDAP="Yes"

# Additionals fix
FIX_SQL="update b_mail_mailbox set ACTIVE=\"N\";"
FIX_DB_SET="Yes"
FIX_DB_CLASS="No"
EOF
exit
fi

if [ ! -f $CONFIG ]
then
    echo "Config file ${CONFIG} not found"
    exit
fi

. $CONFIG

ssh_port="22"
if [[ ! "$REMOTE_PORT" = "" ]]
then
    ssh_port=${REMOTE_PORT}
fi

db_password=""
if [[ ! "$REMOTE_DB_Password" = "" ]]
then
    db_password="-p'${REMOTE_DB_Password}'"
fi

# Prepare command arguments

ignore_cmd=""
for i in "${REMOTE_DB_ignore_table[@]}"
do
    ignore_cmd="$ignore_cmd --ignore-table=$REMOTE_DB_Name.${i}"
done

exclude_cmd=""
for i in "${TAR_exclude[@]}"
do
    exclude_cmd="$exclude_cmd --exclude='${i}'"
done

upload_exclude_cmd=""
for i in "${UPLOAD_exclude[@]}"
do
    upload_exclude_cmd="$upload_exclude_cmd --exclude=${i}/*"
done

if [[ "$OPTdb" = "Yes" ]]
then

    echo "Clone database"

    if [[ "$REMOTE_DB_sync_only" = "Yes" ]]
    then
        echo "REMOTE_DB_sync_only"
        mysql -u $DBLogin -p$DBPassword -N information_schema -e "select table_name from tables where table_schema = '${DBName}' and (table_name like 'b_uts_%' or table_name like 'b_utm_%');" > ~/temp_sync_tables.txt
        SYNC_TABLES=`cat ~/temp_sync_tables.txt | tr '\r\n' ' '`
        SYNC_TABLES="${SYNC_TABLES} b_user_field b_user_field_confirm b_user_field_enum b_user_field_lang b_crm_status b_iblock_fields b_iblock_property b_iblock_property_enum b_iblock_property_feature b_lists_field b_lists_permission b_lists_socnet_group b_lists_url"
        rm ~/temp_sync_tables.txt

        ssh $REMOTE_HOST -p $ssh_port "mysqldump -u$REMOTE_DB_Login $db_password $REMOTE_DB_Name $SYNC_TABLES > temp.sql"
        ssh $REMOTE_HOST -p $ssh_port "mysqldump -u$REMOTE_DB_Login $db_password --no-create-info $REMOTE_DB_Name b_user_option --where=\"category = 'form'\" >> temp.sql"
        ssh $REMOTE_HOST -p $ssh_port "gzip ~/temp.sql"
        scp -P $ssh_port $REMOTE_HOST:temp.sql.gz ~/
        ssh $REMOTE_HOST -p $ssh_port 'rm ~/temp.sql.gz'
        gzip -f -d ~/temp.sql.gz
    else

        if [[ "${KEEP_LDAP}" = "Yes" ]]
        then
            echo "- keep AD/LDAP settings"
            mysqldump -u$DBLogin -p"${DBPassword}" ${DBName} b_ldap_group > ~/temp_ldap_backup.sql
            mysqldump -u$DBLogin -p"${DBPassword}"  ${DBName} b_ldap_server >> ~/temp_ldap_backup.sql
            mysqldump -u$DBLogin -p"${DBPassword}" --no-create-info ${DBName} b_option --where="MODULE_ID = 'ldap'" >> ~/temp_ldap_backup.sql
        fi

        if [ ! -f ~/temp_struct.sql ]
        then
            echo "- export structure..."
            ssh $REMOTE_HOST -p $ssh_port "mysqldump -u$REMOTE_DB_Login $db_password --no-data $REMOTE_DB_Name | gzip -c > temp_struct.sql.gz"
            scp -P $ssh_port $REMOTE_HOST:temp_struct.sql.gz ~/
            ssh $REMOTE_HOST -p $ssh_port 'rm ~/temp_struct.sql.gz'
            gzip -f -d ~/temp_struct.sql.gz
        fi

        if [ ! -f ~/temp.sql ]
        then
            echo "- export data (without big tables)..."
            ssh $REMOTE_HOST -p $ssh_port "mysqldump -u$REMOTE_DB_Login $db_password $ignore_cmd $REMOTE_DB_Name | gzip -c > temp.sql.gz"
            scp -P $ssh_port $REMOTE_HOST:temp.sql.gz ~/
            ssh $REMOTE_HOST -p $ssh_port 'rm ~/temp.sql.gz'
            gzip -f -d ~/temp.sql.gz
        fi

        if [ -f ~/temp_struct.sql ]
        then
            echo "- import structure..."
            mysql $DBName -u $DBLogin -p$DBPassword < ~/temp_struct.sql
            rm ~/temp_struct.sql
        fi
    fi

    if [ -f ~/temp.sql ]
    then
        echo "- import data..."
        mysql $DBName -u $DBLogin -p$DBPassword < ~/temp.sql
        rm ~/temp.sql

        if [[ "${KEEP_LDAP}" = "Yes" ]]
        then
            if [ -f ~/temp_ldap_backup.sql ]
            then
                echo "- restore AD/LDAP settings"
                mysql $DBName -u $DBLogin -p$DBPassword < ~/temp_ldap_backup.sql
                rm ~/temp_ldap_backup.sql
            else
                echo "Warning: temp_ldap_backup.sql not found"
            fi
        fi
    fi

    echo "Done: database cloned"
fi

if [[ "$OPTwww" = "Yes" ]]
then
    echo "Clone www"

    if [ ! -f ~/temp.tar.gz ]
    then
        echo "- tar.gz on remote host..."
        ssh $REMOTE_HOST -p $ssh_port "cd ${REMOTE_WWW}; tar ${exclude_cmd} --exclude-vcs -czf ~/temp.tar.gz ."
        scp -P $ssh_port $REMOTE_HOST:temp.tar.gz ~/
        ssh $REMOTE_HOST -p $ssh_port 'rm ~/temp.tar.gz'
    fi

    if [ -f ~/temp.tar.gz ]
	then
        tar -zxvf ~/temp.tar.gz -C ${WWW}
        rm ~/temp.tar.gz
	else
        echo "Error: temp.tar.gz not found"
	fi
fi

if [[ "$OPTupload" = "Yes" ]]
then
    echo "Upload rsync"
    rsync -vhrx --size-only  -e "ssh -p ${ssh_port}" $upload_exclude_cmd $REMOTE_HOST:$REMOTE_WWW/upload $WWW
fi

if [[ "$OPTfix" = "Yes" ]]
then
    echo "Fix database settings and custom..."

    if [[ ! "${FIX_SQL}" = "" ]]
    then
        echo "Fix custom SQL"
        mysql -u ${DBLogin} -p${DBPassword} ${DBName} -B -e "${FIX_SQL}"
    fi

    echo "Fix SQL"
    mysql -u ${DBLogin} -p${DBPassword} ${DBName} << EOF
update b_mail_mailbox set ACTIVE="N";
update b_option set VALUE="" where name="restriction_hosts_action";
update b_clouds_file_bucket set READ_ONLY="Y";
update b_option set VALUE="Y" where MODULE_ID="main" and NAME="update_devsrv";
EOF

    DBCONN=${WWW}/bitrix/php_interface/dbconn.php

    if [[ "${FIX_DB_SET}" = "Yes" ]]
    then
        echo "Fix DB settings"

        cp ${WWW}/bitrix/.settings.php ${WWW}/bitrix/.settings.php.clone_ori
        cp ${DBCONN} ${DBCONN}.clone_ori

        sed -i "s/['\"]database['\"] => ['\"]${REMOTE_DB_Name}['\"]/'database' => '${DBName}'/" ${WWW}/bitrix/.settings.php
        sed -i "s/['\"]login['\"] => ['\"]${REMOTE_DB_Login}['\"]/'login' => '${DBLogin}'/" ${WWW}/bitrix/.settings.php
        sed -i "s/['\"]password['\"] => ['\"]${REMOTE_DB_Password}['\"]/'password' => '${DBPassword}'/" ${WWW}/bitrix/.settings.php

        sed -i "s/DBLogin = ['\"]${REMOTE_DB_Login}['\"]/DBLogin = \"${DBLogin}\"/" ${DBCONN}
        sed -i "s/DBPassword = ['\"]${REMOTE_DB_Password}['\"]/DBPassword = \"${DBPassword}\"/" ${DBCONN}
        sed -i "s/DBName = ['\"]${REMOTE_DB_Name}['\"]/DBName = \"${DBName}\"/" ${DBCONN}
    fi

    if [[ "${FIX_DB_CLASS}" = "Yes" ]]
    then
        echo "Fix DB MYSQLI"
        sed -i "s/MysqlConnection/MysqliConnection/" ${WWW}/bitrix/.settings.php
        MYSQLI="define(\"BX_USE_MYSQLI\", true);"
        grep -q "${MYSQLI}" ${DBCONN}
        if [ ! $? -eq 0 ]
        then
            echo "mysqli"
            sed -i "s/?>/${MYSQLI}?>/" ${DBCONN}
        fi
    fi
fi

echo "Done"
