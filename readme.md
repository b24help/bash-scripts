# Bash-скрипты для VMBitrix

-   `clone` клонирование сайта с удаленного хоста на локальный
-   `backup_s3` бэкап сайта в облако S3 (aws или hotbox)

## Требования к хосту

Скрипты предназначены для эксплуатации:

-   [«1С-Битрикс: Веб-окружение» - Linux](https://www.1c-bitrix.ru/products/env/)
-   [«1C-Битрикс: Виртуальная машина»](https://www.1c-bitrix.ru/products/vmbitrix/)

Установите pip: `yum -y install python-pip`

[Installing the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)

Для автоматического запуска бэкапа в `crontab -e` добавить строку `30 03 * * * $HOME/backup_s3.sh --all`

## `clone` клонирование сайта

Для удобства эксплуатации сокращенной копии (без папки `upload`) рекомендуется в nginx настройку сайта на локальной виртуальной машине добавить проксирование запросов к upload на удаленный хост:

```
# upload (remote content)
location ~* ^/(upload) {
  return 301 https://rempote_host$request_uri;
}
```

### TODO:

-   Добавить включение режима разработки для лицензии (найти запрос)
-   Отладить замену паролей со спецсимволами (иногда бывают глюки)
-   Актуализировать PUSH-key в `.settings.php`

## `backup_s3` бэкапа сайта

### TODO:

-   Slack notify

## License

`backup_s3` and `clone` is licensed under the MIT License

## Author

Vladimir Tyrtov - [info@b24.help](mailto:info@b24.help) - https://www.b24.help

## Need custom Bitrix24 application?

email: [info@b24.help](mailto:info@b24.help)
